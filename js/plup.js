(function ($) {

function plup_print_error(err, plupInstance) {
  var message = Drupal.t('Unknown error. Please contact site administrator.');

  if (err.code == -20) {
    var message = Drupal.t('Unable to connect remote server. Please check your internet connection and try again.');
  }
  else if (err.code == -601) {
    var allowed_ext = [];
    $(plupInstance.filters).each(function(key, data){
      if (data.hasOwnProperty('extensions')) {
        allowed_ext = data.extensions;
      }
    });

    var message = Drupal.t('The only allowed extensions are: !ext', { '!ext': allowed_ext });
  }
  else if (err.message.length > 0) {
    var message = err.message;
  }

  if ($.colorbox) {
    $.colorbox({html:message});
  }
  else {
    alert(message);
  }
}

// Bind maining of file to selector elements
function plup_main_item(selector) {
  $(selector).bind('click', function(event) {
    var parentDiv = $(this).parent();
    var targetLi = parentDiv.parent();

    if (targetLi.hasClass('main')) {
      return;
    }
    // replace
    var oldLi = targetLi.siblings('.main');
    $(oldLi).before($(targetLi));

    // styles
    oldLi.removeClass('main');
    targetLi.addClass('main');
  });
}

// Bind removing of file to selector elements
function plup_remove_item(selector) {
  $(selector).bind('click', function(event) {
    var parentDiv = $(this).parent();
    var parentLi = parentDiv.parent();
    var parentRoot = parentLi.parent();

    parentLi.hide('fast', function() {
      $(this).remove();

      $(parentRoot).find('li:not(.plup-select)').each(function(index) {
        if (index == 0) {
          $(this).addClass('main');
        }
        $(this).find('input[name$="[weight]"]').val(index);
      });
    });
    parentRoot.trigger('formUpdated');
  });
}

Drupal.behaviors.plup = function(context) {

  // Apply Plupload to each form element instance
  $.each(Drupal.settings.plup, function(plupName, plupInstance) {
    var cid = plupInstance.container;
    var id = '#' + cid;

    // Apply setting only once
    $(id + ':not(.plup)').each(function() {
      $(this).addClass('plup');

      /**
       * Initial tasks
       */
      $(id +' .plup-list').sortable({ // Activate sortable
        items: "li:not(.plup-select)"
      });
      var uploader = new plupload.Uploader(plupInstance); // Create Plupload instance
      plup_remove_item(id +' .plup-remove-item'); // Bind remove functionality on existing images
      plup_main_item(id +' .plup-main-item'); // Bind remove functionality on existing images

      /**
       * Events
       */

      // Initialization
      uploader.bind('Init', function(up, params) {
        // Nothing to do, just reminder of the presence for this event
        $(id + ' .plup-list li').removeClass('main');
        $(id + ' .plup-list li:not(.plup-select):first').addClass('main');
      });

      // Starting upload
      uploader.bind('Start', function(up, files) {
        // Nothing to do, just reminder of the presence for this event
      });

      // Upload process state
      uploader.bind('StateChanged', function(up) {
        // Nothing to do, just reminder of the presence for this event
        if (up.state == 2) {
          $(id + ' .plup-list li.plup-select').addClass('plup-active');
        }
        else {
          $(id + ' .plup-list li.plup-select').removeClass('plup-active');
        }
      })

      // File was removed from queue
      // Doc states this ARE files but actually it IS a file
      uploader.bind('FilesRemoved', function(up, file) {
        // Nothing to do, just reminder of the presence for this event
      });

      // File is being uploaded
      uploader.bind('UploadProgress', function(up, file) {

      });

      // Error event
      uploader.bind('Error', function(up, err) {
        plup_print_error(err, plupInstance);
        up.refresh(); // Refresh for flash or silverlight
      });

      // Event after a file has been uploaded from queue
      uploader.bind('FileUploaded', function(up, file, response) {
        // Response is an object with response parameter so 2x response
        var fileSaved = $.parseJSON(response.response);
        var delta = $(id +' .plup-list li').length;
        var name = plupName +'[' + delta + ']';

        // Plupload has weird error handling behavior so we have to check for errors here
        if (!fileSaved.hasOwnProperty('fid')) {
          plup_print_error(fileSaved, plupInstance);
          up.refresh(); // Refresh for flash or silverlight
          return;
        }

        var alt_field = plupInstance.alt_field ? '<input title="'+ Drupal.t('Alternative text') +'" type="text" class="form-text plup-alt" name="'+ name + '[data][alt]" value="" />' : '';

        // Add image thumbnail into list of uploaded items
        $(
          '<li class="ui-state-default">' +
          '<div class="plup-preview-wrapper"><a class="plup-main-item"></a><a class="plup-remove-item"></a><img src="'+ plupInstance.image_style_path + file.target_name + '" title="'+ Drupal.checkPlain(file.target_name) +'" /></div>' +
          '<div class="plup-fields">' +
          alt_field +
          '</div>' +
          '<input type="hidden" name="' + name + '[fid]" value="' + fileSaved.fid + '" />' +
          '<input type="hidden" name="' + name + '[weight]" value="' + delta + '" />' +
          '<input type="hidden" name="' + name + '[rename]" value="' + file.name +'" />' +
          '</li>').insertAfter(id +' .plup-list li.plup-select');

        // Bind remove functionality to uploaded file
        var new_element = $('input[name="'+ name +'[fid]"]');
        var remove_element = $(new_element).siblings('.plup-preview-wrapper').find('.plup-remove-item');
        plup_remove_item(remove_element);

        var main_element = $(new_element).siblings('.plup-preview-wrapper').find('.plup-main-item')
        plup_main_item(main_element);

        // Tell Drupal that form has been updated
        new_element.trigger('formUpdated');

        $(id + ' .plup-list li').each(function(index) {
          $(this).removeClass('main');
          $(this).find('input[name$="[weight]"]').val(index);
        });
        $(id + ' .plup-list li:not(.plup-select):first').addClass('main');
      });

      // All fields from queue has been uploaded
      uploader.bind('UploadComplete', function(up, files) {
        $(id +' .plup-list').sortable('refresh'); // Refresh sortable
      });

      /**
       * Additional tasks
       */

      // Initialize Plupload
      uploader.init();

      // Files were added into queue(only CURRENTLY added files)
      uploader.bind('FilesAdded', function(up, files) {
        var total = files.length + $(id +' .plup-list li:not(.plup-select)').length;
        if (plupInstance.max_files > 0 && plupInstance.max_files < total) {
          alert(Drupal.t('Maximum files could be uploaded is ' + plupInstance.max_files));
          $(files).each(function(i, file){
            up.removeFile(file);
          });

          return;
        }

        up.start();
      });

      // Change weight values for images when reordering using sortable
      $(id +' .plup-list').bind('sortupdate', function(event, ui) {
        $(this).find('li').each(function(index) {
          $(this).removeClass('main');
          $(this).find('input[name$="[weight]"]').val(index);
        });

        $(this).find('li:not(.plup-select):first').addClass('main');
      });
    });
  });

}

$.extend({
  error: function( msg ) { throw msg; },
  parseJSON: function( data ) {
    if ( typeof data !== "string" || !data ) {
        return null;
    }
    data = jQuery.trim( data );
    if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
        .replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {
        return window.JSON && window.JSON.parse ?
            window.JSON.parse( data ) :
            (new Function("return " + data))();
    } else {
        jQuery.error( "Invalid JSON: " + data );
    }
  }
});

})(jQuery);
