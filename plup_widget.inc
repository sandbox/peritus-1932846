<?php



/**
 * Implementation of CCK's hook_widget_info().
 */
function plup_widget_info() {
  return array(
    'plup_widget' => array(
      'label' => t('Plupload'),
      'field types' => array('filefield'),
      'multiple values' => CONTENT_HANDLE_CUSTOM,
      'callbacks' => array('default value' => CONTENT_CALLBACK_NONE),
    ),
  );
}

/**
 * Implementation of CCK's hook_widget_settings().
 */
function plup_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      return plup_widget_settings_form($widget);
    case 'validate':
      return plup_widget_settings_validate($widget);
    case 'save':
      return plup_widget_settings_save($widget);
  }
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function plup_widget_settings_form($widget) {
  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  if ($form['file_extensions']['#default_value'] == 'txt') {
    $form['file_extensions']['#default_value'] = 'png gif jpg jpeg';
  }

  $presets = array();
  foreach (imagecache_presets() as $preset) {
    $presets[$preset['presetname']] = $preset['presetname'];
  }
  $form['preview_preset'] = array(
    '#type' => 'select',
    '#title' => t('Preview preset'),
    '#default_value' => !empty($widget['preview_preset']) ? $widget['preview_preset'] : 0,
    '#options' => $presets,
    '#weight' => 2,
  );

  $form['resize_method'] = array(
    '#type' => 'radios',
    '#title' => t('Resize method'),
    '#default_value' => !empty($widget['resize_method']) ? $widget['resize_method'] : 0,
    '#options' => array(
      t('Server side (GD)'),
      t('Client side (Plupload)')
    ),
    '#weight' => 2,
  );

  $form['max_resolution'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum resolution for Images'),
    '#default_value' => !empty($widget['max_resolution']) ? $widget['max_resolution'] : 0,
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The maximum allowed image size expressed as WIDTHxHEIGHT (e.g. 640x480). Set to 0 for no restriction. If a larger image is uploaded, it will be resized to reflect the given width and height.'),
    '#weight' => 2.1,
  );

  $form['min_resolution'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum resolution for Images'),
    '#default_value' => !empty($widget['min_resolution']) ? $widget['min_resolution'] : 0,
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The minimum allowed image size expressed as WIDTHxHEIGHT (e.g. 640x480). Set to 0 for no restriction. If an image that is smaller than these dimensions is uploaded it will be rejected.'),
    '#weight' => 2.2,
  );

  if (!module_exists('imageapi') || imageapi_default_toolkit() == 'imageapi_gd') {
    $form['max_resolution']['#description'] .= ' ' . t('Resizing images on upload will cause the loss of <a href="http://en.wikipedia.org/wiki/Exchangeable_image_file_format">EXIF data</a> in the image.');
  }

  $form['alt_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('ALT text settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );
  $form['alt_settings']['custom_alt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom alternate text'),
    '#default_value' =>  !empty($widget['custom_alt']) ? $widget['custom_alt'] : 0,
    '#description' => t('Enable user input alternate text for images.'),
  );
  $form['alt_settings']['alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Default ALT text'),
    '#default_value' => !empty($widget['alt']) ? $widget['alt'] : '',
    '#description' => t('This value will be used for alternate text by default.'),
    '#suffix' => theme('token_help', array('user')),
  );

  return $form;
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'validate').
 */
function plup_widget_settings_validate($widget) {
  // Check that only web images are specified in the callback.
  $extensions = array_filter(explode(' ', $widget['file_extensions']));
  $web_extensions = array('jpg', 'jpeg', 'gif', 'png');
  if (count(array_diff($extensions, $web_extensions))) {
    form_set_error('file_extensions', t('Only web-standard images (jpg, gif, and png) are supported through the image widget. If needing to upload other types of images, change the widget to use a standard file upload.'));
  }

  // Check that set resolutions are valid.
  foreach (array('min_resolution', 'max_resolution') as $resolution) {
    if (!empty($widget[$resolution]) && !preg_match('/^[0-9]+x[0-9]+$/', $widget[$resolution])) {
      form_set_error($resolution, t('Please specify a resolution in the format WIDTHxHEIGHT (e.g. 640x480).'));
    }
  }
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */
function plup_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);
  return array_merge($filefield_settings, array('preview_preset', 'resize_method', 'max_resolution', 'min_resolution', 'alt',  'custom_alt'));
}

/**
 * Implementation of CCK's hook_widget().
 *
 * Assign default properties to item and delegate to FileField.
 */
function plup_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $default_values = $items;

  $images = array(
    '#type' => 'plupload_file',
    '#default_value' => $default_values,
  );

  if (isset($field['widget']['preview_preset']) && !empty($field['widget']['preview_preset'])) {
    $images['#plup_override']['image_style'] = $field['widget']['preview_preset'];
  }

  // Allowed file types(extensions) needs to be set here
  $ext = new stdClass();
  $ext->title = 'Allowed extensions'; // This won't show up anywhere so no t()
  $ext->extensions = (isset($field['widget']['file_extensions']) && !empty($field['widget']['file_extensions'])) ? strtr($field['widget']['file_extensions'], ' ', ',') : 'jpg,png,gif';
  $images['#plup_override']['filters'] = array($ext);
  // Maximal file size
  if (isset($field['widget']['max_filesize_per_node']) && !empty($field['widget']['max_filesize_per_node'])) {
    $images['#plup_override']['max_file_size'] = $field['widget']['max_filesize_per_node'];
  }
  if (isset($field['widget']['resize_method']) && $field['widget']['resize_method'] && $field['widget']['max_resolution']) {
    $size = explode('x', $field['widget']['max_resolution']);
    $images['#plup_override']['resize'] = array('width' => $size[0], 'height' => $size[1], 'quality' => 90);
  }
  // URL callback for Plupload library has to be altered so we can get instance ID for later validation
  $images['#plup_override']['url'] = url('plupload/'. $field['field_name'], array('query' => array('plupload_token' => drupal_get_token('plupload-handle-uploads'))));
  // We set if we want to enable title and alt fields for images
  $images['#plup_override']['alt_field'] = (int) $field['widget']['custom_alt'];

  // We set the maximum files user can upload
  $cardinality = $field['multiple'];
  if ($field['multiple'] == 0)
    $cardinality = 1;
  else if ($field['multiple'] == 1)
    $cardinality = -1;

  $images['#plup_override']['max_files'] = (int) $cardinality;

  $form['#submit'][] = 'plup_widget_submit';
  $form['#plup_fields'][] = $field['field_name'];

  return $images;
}

/**
 * Save files to filefield.
 */
function plup_widget_submit(&$form, &$form_state) {
  foreach ($form['#plup_fields'] as $field_name) {
    $element = $form[$field_name];
    $field = content_fields($element['#field_name'], $element['#type_name']);
    $destination = filefield_widget_file_path($field);
    field_file_check_directory($destination, FILE_CREATE_DIRECTORY);

    foreach ($form_state['values'][$field_name] as $key => $item) {
      $file = (object) field_file_load($item['fid']);
      $file->filename = isset($item['rename']) ? $item['rename'] : $file->filename;
      $filePath = $destination . '/' . $file->filename;
      if ($file->filepath !== $filePath) {
        file_move($file->filepath, $filePath);
        $file->status = 1;
        drupal_write_record('files', $file, array('fid'));
      }
    }
  }
}
