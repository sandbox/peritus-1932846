<?php

/**
 * Implementation of hook_elements().
 */
function plup_elements() {
  $plupload = plup_get_library_path();

  $types['plupload_file'] = array(
    // default attributes
    '#input' => TRUE,
    '#autocomplete_path' => FALSE,
    '#process' => array('plup_element_process'),
    '#value_callback' => 'plup_element_value',
    '#element_validate' => array('plup_element_validate'),
    '#pre_render' => array('plup_element_pre_render'),
    '#theme' => 'plupload_file',
    // plup element specific attributes
    '#info' => array(),
    '#plup' => array(
      'container' => NULL,
      'browse_button' => NULL,
      'upload' => NULL,
      'runtimes' => 'html5,gears,flash,silverlight,browserplus,html4',
      'max_file_size' => '512MB',
      'url' => url('plupload', array('query' => array('plupload_token' => drupal_get_token('plupload-handle-uploads')))),
      'filters' => array(),
      'chunk_size' => '512K',
      'unique_names' => TRUE,
      'flash_swf_url' => base_path() . $plupload . '/js/plupload.flash.swf',
      'silverlight_xap_url' => base_path() . $plupload . '/js/plupload.silverlight.xap',
      'drop_element' => NULL,
      'multipart' => FALSE,
      'dragdrop' => TRUE,
      'multiple_queues' => TRUE,
      'urlstream_upload' => FALSE,
      'resize' => FALSE,
      'image_style' => 'plup_preview', // imagecache preset
      'image_style_path' => '',
      'max_files' => -1,
    ),
    '#plup_override' => array(),
  );

  return $types;
}

/**
 * Process callback to set JS settings before Plupload init.
 */
function plup_element_process($element) {
  $element['#default_value'] = isset($element['#value'])
    ? $element['#value']
    : $element['#default_value'];

  $element['#plup']['container'] = $element['#id'];
  $element['#plup']['browse_button'] = $element['#id'] .'-plup-select';

  $element['#plup']['name'] = $element['#name'];
  $element['#plup'] = array_merge($element['#plup'], $element['#plup_override']);
  $element['#plup']['image_style_path'] = base_path() . file_directory_path() .'/imagecache/'. $element['#plup']['image_style'] .'/tmp/';

  plup_element_add_js_settings($element['#name'], $element['#plup']);
  return $element;
}

function plup_element_add_js_settings($name, $plup) {
  static $settings = array();

  if (!isset($settings[$name])) {
    drupal_add_js(array('plup' => array($name => $plup)), 'setting');

    $settings[$name] = TRUE;
  }
}

/**
 * Helper function to attach frontend files.
 */
function plup_attach_frontend() {
  static $attached;

  if (!$attached) {
    $path = drupal_get_path('module', 'plup');
    drupal_add_js(plup_get_library_path() . "/js/plupload.full.js");
    drupal_add_js("$path/js/plup.js");
    drupal_add_css("$path/css/plup.css");

    jquery_ui_add(array('ui.sortable'));
  }
}

/**
 * Value callback needed for removing all items.
 */
function plup_element_value($element, $input = FALSE, $form_state = NULL) {
  // Default state - no new data
  if ($input === FALSE) {
    return NULL;
  }
  // Field was emptied - user deleted all files
  if (is_null($input)) {
    return array(array('fid' => 0));
  }

  // Field has new data
  return $input;
}


/**
 * Pre-render callback to load existing items.
 */
function plup_element_pre_render($element) {
  if (isset($element['#default_value']) && !empty($element['#default_value'])) {
    foreach ($element['#default_value'] AS $delta => $item) {
      if (isset($item['fid'])) {
        $element['#default_value'][$delta] = array_merge($item, (array)db_fetch_array(db_query('SELECT * FROM {files} WHERE fid = %d', $item['fid'])));
      }
    }
  }
  return $element;
}

/**
 * Element validation callback.
 */
function plup_element_validate($element, &$form_state, $form) {
  $item = reset($element['#value']);
  if ($element['#required'] == TRUE && $item['fid'] == 0 && count($element['#value']) == 1) {
    form_error($element, t("@field field is required.", array('@field' => $element['#title'])));
  }

  $cardinality = isset($element['#plup_override']['max_files']) ? $element['#plup_override']['max_files'] : $element['#plup']['max_files'];
  if ($cardinality > 0 && count($element['#value']) > $cardinality) {
    form_error($element, t("Only !num items are allowed.", array('!num' => $cardinality)));
  }
}

/**
 * Theme Plupload widget.
 */
function theme_plupload_file($element) {
  plup_attach_frontend();

  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'plupload form-item';

  $hasAlt = (bool) (isset($element['#plup_override']['alt_field']) && $element['#plup_override']['alt_field'] == 1);
  if ($hasAlt) {
    $attributes['class'][] = 'has-alt';
  }

  $attributes['class'] = implode(' ', $attributes['class']);

  $output = '<div' . drupal_attributes($attributes) . '>';

  // Output label
  $required = !empty($element['#required']) ? '<span class="form-required" title="' . t('This field is required.') . '">*</span>' : '';
  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="' . $element['#id'] . '">' . t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
    else {
      $output .= ' <label>' . $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
  }

  $output .= '<div id="'. $element['#id'] .'-plupload-container" class="plupload-container">';
  $output .= '<div class="plup-list-wrapper"><ul class="plup-list clearfix">';
  $output .= '<li class="plup-select"><div id="'. $element['#id'] .'-plup-select"></div></li>';
  $output .= theme('plupload_file_items', $element);
  $output .= '</ul><div style="clear:both;"></div></div>';
  $output .= '</div>';

  $output .= '</div>';
  return $output;
}

/**
 * Theme Plupload items within widget.
 */
function theme_plupload_file_items($element) {
  if (isset($element['#default_value']) && !empty($element['#default_value'])) {
    $items = &$element['#default_value'];
  } else {
    return '';
  }

  foreach ($items AS $delta => $item) {
    // If user deleted all items I'll get array('fid' => 0)
    if ($item['fid'] > 0) {
      $hasAlt = (bool) (isset($element['#plup_override']['alt_field']) && $element['#plup_override']['alt_field'] == 1);
      $name = $element['#name'] .'['. $delta .']';
      $output .= "<li>";
      $output .= '<div class="plup-preview-wrapper"><a class="plup-main-item"></a><a class="plup-remove-item"></a>'.theme('imagecache', $element['#plup_override']['image_style'], $item['filepath'], $item['filename'], $item['filename']) .'</div>';
      if ($hasAlt) {
        $output .= '<div class="plup-fields"><input title="'. t('Alternative text') .'" type="text" class="form-text plup-alt" name="'. $name .'[data][alt]" value="'. $item['data']['alt'] .'" /></div>';
      }
      $output .= '<input type="hidden" name="'. $name .'[fid]" value="'. $item['fid'] .'" />';
      $output .= '<input type="hidden" name="'. $name .'[weight]" value="'. $delta .'" />';
      if (isset($item['rename'])) {
        $output .= '<input type="hidden" name="'. $name .'[rename]" value="'. $item['rename'] .'" />';
      }
      $output .= '</li>';
    }
  }
  return $output;
}
